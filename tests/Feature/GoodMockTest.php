<?php

namespace Tests\Feature;

use App\Models\Good;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GoodMockTest extends TestCase
{
    public function testBasic()
    {
        $stub = $this
            ->getMockBuilder(Good::class)
            ->disableOriginalConstructor()
            ->getMock();

        $stub->method('getGoodByID')
            ->willReturn(['id' => '1', 'country' => 'Test', 'song_title' => 'Test', 'rating' => 'Test']);

        $this->assertEquals('Test', $stub->getGoodByID(1)['country']);
    }
}
