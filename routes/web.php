<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GoodController;

Auth::routes();
Route::get('/', [GoodController::class, 'getList']);
Route::resource('goods', GoodController::class);

