<?php

namespace App\Http\Controllers;

use App\Models\Good;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class GoodController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['getList']);
    }

    public function getList(){
        return view('list', ['goods' => Good::all()]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Request $request)
    {
        $query = Good::query();

        if(filled($request->title))
            $query->where('title', 'LIKE', '%'.$request->title.'%');

        switch ($request->sort){
            case '1':
                $query->orderBy('title');
                break;
            case '2':
                $query->orderByDesc('price');
                break;
            default:
                break;
        }

        return view('admin.view', ['request' => $request, 'goods' => $query->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $good = new Good();

        $good->title = $request->title;
        $good->country = $request->country;
        $good->date = $request->date;
        $good->price = $request->price;

        $good->save();

        return Redirect::to('/goods');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function show(Good $good)
    {
        return view('good', ['good' => $good]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function edit(Good $good)
    {
        return view('admin.edit', ['good' => $good]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Good $good)
    {
        $good->title = $request->title;
        $good->country = $request->country;
        $good->date = $request->date;
        $good->price = $request->price;

        $good->save();

        return Redirect::to('/goods');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Good  $good
     * @return \Illuminate\Http\Response
     */
    public function destroy(Good $good)
    {
        $good->delete();
        return Redirect::to('/goods');
    }
}
