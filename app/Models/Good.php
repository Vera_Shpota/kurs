<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Good extends Model
{
    use HasFactory;

    public const SORT_TYPES = [
        '1' => 'за назвою',
        '2' => 'за вартістю',
    ];

    public const COUNTRIES = [
        '1' => 'UA',
        '2' => 'POL',
        '3' => 'RU',
        '4' => 'DE',
    ];

    public function getGoodByID($id){
        return Good::all()->find($id);
    }

    public $timestamps = false;
    protected $primaryKey = 'id';
    public $incrementing = true;
    protected $table = 'goods';

    protected $fillable = [
        'title',
        'country',
        'date',
        'price',
    ];
}
