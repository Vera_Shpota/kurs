<h1>Інформація:</h1>
<table>
    <tr>
        <td>ID</td>
        <td>Назва препарату</td>
        <td>Дата виготовлення</td>
        <td>Ціна</td>
    </tr>
    <tr>
        <td>{{$good->id}}</td>
        <td><a href="/goods/{{$good->id}}">{{$good->title}}</a></td>
        <td>{{$good->date}}</td>
        <td>{{$good->price}}</td>
    </tr>
</table>
<a href="/goods">Повернутись</a>
