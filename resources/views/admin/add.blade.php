<h1>Додати препарат</h1>

<form action="/goods" method="post">
    @csrf
    <span>Назва препарату: <input type="text" name="title" placeholder="Назва товару"></span> <br><br>
    <span>Країна:
        <select name="country">
            <option value="">Не обрано</option>
            @foreach(\App\Models\Good::COUNTRIES as $country)
                <option value="{{$country}}">{{$country}}</option>
            @endforeach
        </select>
    </span> <br><br>
    <span>Дата виготовлення: <input type="date" name="date"></span> <br><br>
    <span>Ціна за одну одиницю: <input type="number" name="price"></span> <br><br>
    <input type="submit" value="Додати">
</form>
