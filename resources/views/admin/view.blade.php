@extends('layouts.app')
<h1>Панель керування Аптекою</h1>

<form>
    <span>Пошук: <input type="text" name="title" placeholder="Назва препарату"></span>

    <span>Сортувати:
        <select name="sort">
            <option value="">Не сортувати</option>
            @foreach(\App\Models\Good::SORT_TYPES as $key => $type)
                <option value="{{$key}}" {{($request->sort == $key) ? 'selected' : ''}}>{{$type}}</option>
            @endforeach
        </select>
    </span>

    <input type="submit" value="Відобразити" class="btn btn-success">
</form>
<a href="/goods">Скинути фільтри</a><br><br><a href="/goods/create" class="btn btn-info">Додати препарат</a><br><br>

<table border="1">
    <tr>
        <td>ID</td>
        <td>Назва препарату</td>
        <td>Країна</td>
        <td>Дата виготовлення</td>
        <td>Ціна</td>
        <td>Редагувати</td>
        <td>Видалити</td>
    </tr>
    @foreach($goods as $good)
        <tr>
            <td>{{$good->id}}</td>
            <td><a href="/goods/{{$good->id}}">{{$good->title}}</a></td>
            <td>{{$good->country}}</td>
            <td>{{$good->date}}</td>
            <td>{{$good->price}}</td>
            <td><a href="/goods/{{$good->id}}/edit" class="btn btn-warning form-control">Edit</a></td>
            <form action="/goods/{{$good->id}}" method="post">
                @csrf
                @method('DELETE')
                <td><input type="submit" class="btn btn-danger form-control" value="Delete"></td>
            </form>
        </tr>
    @endforeach
</table>
<br>
<a href="/">На головну</a>
<form action="{{ route('logout') }}" method="POST">
    @csrf
    <input type="submit" value="Завершити сеанс">
</form>

