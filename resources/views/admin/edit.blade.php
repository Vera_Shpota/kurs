<h1>Редагувати препарат</h1>

<form action="/goods/{{$good->id}}" method="post">
    @csrf
    @method('PUT')
    <span>Назва препарату: <input type="text" name="title" placeholder="Назва товару" value="{{$good->title}}"></span> <br><br>
    <span>Країна:
        <select name="country">
            <option value="">Не обрано</option>
            @foreach(\App\Models\Good::COUNTRIES as $country)
                <option value="{{$country}}" {{($good->country == $country) ? 'selected' : ''}}>{{$country}}</option>
            @endforeach
        </select>
    </span> <br><br>
    <span>Дата виготовлення: <input type="date" name="date" value="{{$good->date}}"></span> <br><br>
    <span>Ціна за одну одиницю: <input type="number" name="price" value="{{$good->price}}"></span> <br><br>
    <input type="submit" value="Редагувати">
</form>
