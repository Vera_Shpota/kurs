@extends('layouts.app')
<title>Система Аптека</title>
<h1>Аптека</h1>
<b>Список товарів:</b>
<table border="1">
    <tr>
        <td>ID</td>
        <td>Назва препарату</td>
        <td>Країна</td>
        <td>Дата виготовлення</td>
        <td>Ціна</td>
    </tr>
    @foreach($goods as $good)
        <tr>
            <td>{{$good->id}}</td>
            <td><a href="/goods/{{$good->id}}">{{$good->title}}</a></td>
            <td>{{$good->country}}</td>
            <td>{{$good->date}}</td>
            <td>{{$good->price}}</td>
        </tr>
    @endforeach
</table>

<a href="/goods">Панель керування препаратами</a><br>
<a href="/login">Увійти</a><br>
<a href="/register">Зареєструватись</a>
